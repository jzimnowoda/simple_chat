<?php
/**
 * Created by PhpStorm.
 * User: j.zimnowoda
 * Date: 3/5/18
 * Time: 7:09 PM
 */


class ContentType {
    static public $application_json = 'application/json';
}


class Request {

    public $http_uri;
    public $http_method;
    public $http_body;
    public $http_content_type;
    public $http_authorization;

    /**
     * Request constructor.
     * @param $server array
     * @throws Exception
     */
    public function __construct($server) {
        $this->http_method = $server["REQUEST_METHOD"];
        $this->http_uri = $server["REQUEST_URI"];
        $this->http_authorization = $server['HTTP_AUTHORIZATION'];
        $this->http_content_type = $server['HTTP_CONTENT_TYPE'];
    }

    private function get_http_body() {
        if ($this->http_content_type !== ContentType::$application_json) {
            throw new HttpExceptionBadRequest("Only application/json ContentType is supported");
        }
        $str = file_get_contents('php://input');
        return json_decode($str, True);

    }
}