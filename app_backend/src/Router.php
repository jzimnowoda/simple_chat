<?php
/**
 * Created by PhpStorm.
 * User: j.zimnowoda
 * Date: 3/5/18
 * Time: 2:54 PM
 */


class HttpErrorCode {
    static $BadRequest = 400;
    static $Unauthorized = 401;
    static $PageNotFound = 404;
}

class HttpExceptionBadRequest extends Exception {
    public function __construct($message, Throwable $previous = null)
    {
        parent::__construct($message, HttpErrorCode::$BadRequest, $previous);
    }
}

class HttpExceptionUnauthorized extends Exception {
    public function __construct($message, Throwable $previous = null)
    {
        parent::__construct($message, HttpErrorCode::$Unauthorized, $previous);
    }
}
class HttpExceptionPageNotFound extends Exception {
    public function __construct($message, $code=404, Throwable $previous = null)
    {
        parent::__construct($message, HttpErrorCode::$PageNotFound, $previous);
    }
}

class Router
{
    /**
     * @var array Endpoint
     */
    private $endpoints;

    function __construct() {
        print "Create router instance\n";
        $this->endpoints = array();
    }

    /**
     * @param $request Request
     * @return Endpoint
     * @throws HttpExceptionPageNotFound
     * @throws HttpExceptionUnauthorized
     */
    public function get_endpoint($request) {

        $endpoint = $this->find_endpoint($request);
        $this->authorize($request, $endpoint);
        return $endpoint;
    }

    public function add_endpoint($endpoint) {
        array_push($this->endpoints, $endpoint);
    }

    /**
     * @param $request Request
     * @return Endpoint
     * @throws HttpExceptionPageNotFound
     */
    public function find_endpoint($request) {


        foreach ($this->endpoints as $endpoint)
        {
            if ($endpoint->http_method === $request->http_method) {
                if ($this->validate_uri_with_pattern($request->http_uri, $endpoint->uri_pattern))
                    return $endpoint;
            }
        }

        throw new HttpExceptionPageNotFound('Cannot find requested uri and method');
    }

        /**
         * @param $uri string
         * @param $uri_pattern string
         * @return false|int
         */
    static public function validate_uri_with_pattern($uri, $uri_pattern) {
        return preg_match($uri_pattern, $uri);
    }

    public function authorize($request) {
        throw new HttpExceptionUnauthorized('Cannot find requested uri and method');
    }
}
