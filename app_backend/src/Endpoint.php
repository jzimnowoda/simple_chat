<?php
/**
 * Created by PhpStorm.
 * User: j.zimnowoda
 * Date: 3/5/18
 * Time: 7:08 PM
 */


class Endpoint
{
    public $uri_pattern;
    public $http_method;
    public $content_type;
    public $is_token_required;
    public $callback;

    /**
     * endpoint constructor.
     * @param $uri_pattern: Uniform Resource Identifier to be matched on HTTP request
     * @param $method: Expected HTTP method, e.g.: GET, PATCH, PUT, POST
     * @param $is_token_required: Indicates if authorization is required to perform this request
     * @param $callback:  A callback function to be triggered on proper request
     */
    public function __construct($uri_pattern, $method, $is_token_required, $callback)
    {
        $this->uri_pattern = $uri_pattern;
        $this->http_method = $method;
        $this->is_token_required = $is_token_required;
        $this->callback = $callback;
    }

    /**
     * @param $request Request
     * @return Response
     */
    public function handle_request($request) {
        if ( isset( $this->callback ) ) {
            if (function_exists( $this->callback ))
                return call_user_func( $this->callback, $request);
        }
    }
}
