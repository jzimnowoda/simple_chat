<?php
/**
 * Created by PhpStorm.
 * User: j.zimnowoda
 * Date: 3/5/18
 * Time: 2:54 PM
 */


class ContentType {
    static public $application_json = 'application/json';
}


class Request {

    public $http_uri;
    public $http_method;
    public $http_body;
    public $http_content_type;
    public $token;

    public function __construct() {
        $this->http_method = $_SERVER["REQUEST_METHOD"];
        $this->http_uri = $_SERVER["REQUEST_URI"];
        if (isset($_SERVER['HTTP_AUTHORIZATION']))
            $this->token = explode(' ', $_SERVER['HTTP_AUTHORIZATION']);
        $this->token = null;
        $this-> http_body = $this->get_body();

    }
    public function get_uri_ids() {

    }

    private function get_body() {

        if ($this->http_content_type === ContentType::application_json) {
            $str = file_get_contents('php://input');
            return json_decode($str, True);
        }

        throw Exception("Not supported content type");
    }
}


class Endpoint
{
    public $uri;
    public $http_method;
    public $content_type;
    public $is_token_required;
    public $callback;

    /**
     * endpoint constructor.
     * @param $uri: Uniform Resource Identifier to be matched on HTTP request
     * @param $method: Expected HTTP method, e.g.: GET, PATCH, PUT, POST
     * @param $content_type: Expected content type to be included in HTTP header, e.g.: 'application/json'
     * @param $is_token_required: Indicates if authorization is required to perform this request
     * @param $callback:  A callback function to be triggered on proper request
     */
    public function __construct($uri, $method, $content_type, $is_token_required, $callback)
    {
        $this->uri = $uri;
        $this->http_method = $method;
        $this->content_type = $content_type;
        $this->is_token_required = $is_token_required;
        $this->callback = $callback;
    }

    /**
     * @param $request Request
     */
    public function call_callback_function($request) {
        if ( isset( $this->callback ) ) {
            if (function_exists( $this->callback )) call_user_func( $this->callback, $request);
        }
    }
}


class ExceptionBadRequest extends Exception {
    public function __construct($message, $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, 400, $previous);
    }

}


class Router
{
    private $endpoints;

    function __construct() {
        print "Create router instance\n";
        $this->endpoints = array();
    }

    public function route_request($request) {

        $endpoint = $this->get_endpoint($request);
        $this->authorize($request, $endpoint);
        return $this->render($request, $endpoint);
    }

    public function get_endpoint($request) {
        $size = count($this->endpoints);
        $endpoint_slice = null;
        for($i = 0, $i < $size; $i++;) {
            if ($this->endpoints[$i]->method === $request->method &&$this->endpoints[$i]->uri === $request->uri) {
                return $this->endpoints[$i];
            }
            throw new RouterExceptionBadRequest('Cannot find requested uri and method');
        }
    }

    public function authorize($request) {

    }

    /**
     * @var $endpoint \Endpoint
     */
    public function register_endpoint ($endpoint){
        $this->endpoints->push($endpoint);
    }

    /**
     * @param $request Request
     * @param $endpoint Endpoint
     * @return mixed
     */
    public function render($request, $endpoint) {
        return $endpoint->call_callback_function($request);
    }
}
