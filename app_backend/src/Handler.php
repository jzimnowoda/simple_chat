<?php
/**
 * Created by PhpStorm.
 * User: j.zimnowoda
 * Date: 3/6/18
 * Time: 3:50 PM
 */


class Handler
{
    public function __construct()
    {
        /**
         * @param $request
         * @return Response
         */
        $this->handler_login = function ($request){
            return null;
        };
        /**
         * @param $request
         * @return Response
         */
        $this->save_message = function($request){
            return null;
        };
        /**
         * @param $request
         * @return Response
         */
        $this->get_message_collection = function($request){
            return null;
        };
        /**
         * @param $request
         * @return Response
         */
        $this->create_talk = function($request){
            return null;
        };
        /**
         * @param $request
         * @return Response
         */
        $this->get_talk_collection = function($request){
            return null;
        };
    }
}