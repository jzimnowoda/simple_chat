<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class TestRouter extends TestCase
{
    public function testShouldThrowExceptionDueToReferingToUriThatDoesNotExist(): void
    {
        $server = array (
            "REQUEST_URI" => "/A/1",
        );

        $router = new Router();
        $router->add_endpoint(new Endpoint('~^/B/(\d+)$~', 'GET', false, null));
        $request = new Request($server);
        $this->expectExceptionCode(HttpErrorCode::$PageNotFound);
        $router->find_endpoint($request);
    }

    public function testShouldValidateUri(): void {
        $this->assertEquals(true, Router::validate_uri_with_pattern("/A/1", '~^/A/(\d+)$~'));
    }


    public function testShouldReturnEndpoint(): void
    {
        $server = array (
            "REQUEST_METHOD" => "GET",
            "REQUEST_URI" => "/A/1",
        );

        $router = new Router();
        $endpoint1 = new Endpoint('~^/A/(\d+)$~', 'GET', false, null);
        $endpoint2 = new Endpoint('~^/B/(\d+)$~', 'GET', false, null);
        $router->add_endpoint($endpoint1);
        $router->add_endpoint($endpoint2);
        $request = new Request($server);

        $this->assertEquals($endpoint1, $router->find_endpoint($request));
    }
    public function testShouldThrowExceptionOnUnauthorizedAccess(): void
    {
        $server = array (
        );
        $router = new Router();
        $request = new Request($server);
        $this->expectExceptionCode(HttpErrorCode::$Unauthorized);
        $router->authorize($request);
    }

    public function testShouldThrowExcptionDue(): void
    {
        $server = array (
        );
        $router = new Router();
        $request = new Request($server);
        $this->expectExceptionCode(HttpErrorCode::$Unauthorized);
        $router->authorize($request);
    }


}
